require 'active_support/concern'

module StepsConcern
  extend ActiveSupport::Concern

  class_methods do
    def game_steps
      steps_file = File.read('app/games_steps.json')
      JSON.parse(steps_file)
    end
  end
end
