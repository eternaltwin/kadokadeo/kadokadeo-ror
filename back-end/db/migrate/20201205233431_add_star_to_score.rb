class AddStarToScore < ActiveRecord::Migration[6.0]
  def change
    add_column :scores, :star, :integer, default: 0
  end
end
