import { createDecorator } from 'vue-class-component';

import { mapState } from 'vuex';

// /!\ Doesn't handle modules
export function MapState (states: string[]) {
  return createDecorator(options => {
    if (!options.computed) {
      options.computed = {}
    }
    Object.assign(options.computed, mapState(states))
  })
}
