export default {
  'ABC Revolution' : "Protégez Nounours des lettres mutantes en tapant rapidement sur votre clavier. Enchaînez les combos et marquez des points !",

  'Alchimie' : "Alchimie revient dans une nouvelle version ! Vous ne connaissez pas encore ? Alors enfilez votre robe d'alchimiste et lancez-vous dans la manipulation des éléments.",

  'Alphabounce' : "Grâce à votre raquette métallique, détruisez toutes les briques du niveau et lancez des attaques destructrices grâce aux options lettrées de l'arc-en-ciel ! Ne laissez pas la balle s'échapper !!",

  'Animoz' : "De magnifiques petits animaux ont été enfermés dans un enclos et vous avez le pouvoir de les sauver.<br/><br/>Alignez les par 4 minimum pour les sauver.<br/><br/>A chaque tour de jeu, de nouveaux animaux entrent dans l'enclos.<br/><br/>Mais attention, aussi mignons ces animaux semblent paraître, leur cohabitation peut néanmoins être délicate à gérer.<br/><br/>Chaque race d'animoz possède son propre pouvoir, à vous de bien réussir à les exploiter !",

  'Aqua Splash' : "Le gouffre de Flakra est envahi par des bulles d'eau évolutives.<br>Nettoyez tout ça en les faisant exploser en chaîne !",

  'Atlanteine' : "Aidez la citrouille maudite à s'échapper des limbes d'Atlanteïne en faisant fonctionner vos méninges pour découvrir le chemin le plus court vers la sortie!",

  'Autrement' : "Grâce à votre tampon-magnétiseur rotatif cranté, repoussez les assauts des gemmes infernales avant qu'elles n'envahissent totalement votre espace vital !",

  'Bactery' : "Danger ! Vous êtes attaqué par des bactéries qui se reproduisent rapidement en mangeant vos billes colorées. Vous devez aligner trois billes colorées pour produire des billes grises. Entourez les bactéries de billes grises pour stopper leur reproduction !",

  'BadaBloom' : "Reliez les arbres de la même couleur.<br/><br/>Plus vous faites de croisements entre racines, plus vous générez de fleurs et donc de points.<br/><br/>Faites un croisement sur les bonus et les points KDO pour les valider.",

  'BattleSheep' : "Le réchauffement climatique oblige les manchots à envahir nos vertes contrées, mais les moutons ne l'entendent pas de cette oreille !<br/><br/>Conquérez le maximum d'enclos vides et reprenez par la force les enclos conquis par votre adversaire.<br/><br/>Des bâtiments aux effets variés vous faciliteront la tâche, mais attention l'ennemi peut aussi en profiter !",

  'Binary' : "Une intelligence artificielle terroriste s'est infiltrée chez notre hébergeur et a saboté les serveurs de Kadokado ! Rassemble les modules colorés par groupes de quatre pour remettre le processeur en marche.",

  'Brutal Teenage Crisis' : "Carrie fait sa crise d'adolescence.<br/><br/>Sauf que, contrairement aux autres jeunes filles de son âge, Carrie a un fléau d'arme et compte bien s'en servir pour exprimer tout son désarroi.<br/><br/>Mais Carrie a quand même un bon fond. Elle a donc décidé de canaliser sa rage destructrice pour défendre une juste cause : arrêter les vilains Brutobears qui tentent de coloniser notre monde depuis les profondeurs indicibles.<br/><br/>(Oscar du meilleur scénario 2014)",

  'Capman' : "Mika ne se doutait pas qu'en allumant la vieille console Akari de son paternel il se retrouverait propulsé dans un monde 16Bits pixélisé.<br/>Pour sortir de là, il vous faudra devenir CapMan, l'homme casquette (et ça c'est chouette).<br/><br/>Mode Level-Up<br/><br/>Découvrez les méandres des labyrinthes 16Bits et échappez aux nombreux monstres jonchant chaque recoin !<br/><br/>Mode League<br/><br/>Ramassez un max de points d'affilée pour augmenter votre multiplicateur.",

  'Cataclismo' : "Le serpent à plumes est bien décidé à mettre fin au monde tel que nous le connaissons. Empêchez ses vils desseins en résolvant les énigmes du calendrier aztèque.",

  'Cereal Punk' : "Condamné aux travaux d'intérêt général pour dégradation de la voie publique, Rémi le Punk se voit confier la tâche de trier un grenier rempli de graines de céréales. Aidez-le à rassembler les graines identiques avant qu'elles n'atteignent la limite !",

  'Chakré Boudha' : "Le Chakré Bouddha a besoin de vous pour atteindre le nirvana !<br>Aidez-le à ouvrir ses chakras le plus possible avant que l'énergie ne le submerge.",

  "Charlotte's Quest" : "Aidez Charlotte à retrouver son petit frère Ellon perdu dans ses cauchemars.<br/>Depuis, les forces du Noir Absolu ont gagné en puissance ! Mais elles ne feront jamais le poids contre le Rose Absolu...<br/><br/>Mode LevelUp :<br/><br/>Ramasser des pièces pour acheter des améliorations dans les boutiques Jessicat : tirs, vitesse,...<br/><br/>Terminer un niveau en éliminant les monstres et ramassant les pièces autour du réveil.<br/><br/>Mode League :<br/><br/>Ramasser des billets pour augmenter votre score<br/><br/>Ramasser les bulles roses pour obtenir des améliorations<br/><br/>Aller le plus loin possible, survivez le plus longtemps pour faire le meilleur score et battre vos amis !",

  'Choco-Mouche' : "Une infiltration de grosses mouches noirâtres a contaminé certaines plaques de chocolat en fabrication. <br> Vous avez été désigné pour contrôler les plaques ! Des millions d'enfants attendent leur crise de foie de fin d'année, alors ne les décevez pas ! <br> Pour chaque plaque de chocolat, séparez les carrés sains et ne touchez pas à ceux qui contiennent une mouche.",

  'Cooking Lili' : "Aidez Lili à réaliser le meilleur gâteau du monde en faisant le tri dans tous les délicieux ingrédients ! Assemblez les ingrédients identiques pour marquer des points !",

  'Cosmo Crash' : "L'armée des Storgz vient de débarquer sur le sol de notre colonie ! Nous n'avons plus qu'un seul choix : la fuite. Aidez les colons isolés à rejoindre les plate-formes de lancement.<br>Attention : des tanks Storgz lourdement armés ont déjà été envoyés vers notre position.",

  'Crepuscud' : "Les storgz ont localisé notre centre de détention souterrain ! Prenez les commandes de notre nouvelle batterie de tir anti-missile et empêchez leurs projectiles d'atteindre le sol.",

  'Cyclopean' : "Pioupiou est enfermé dans un tunnel mystique d'herbes épaisses ! Aidez-le à accumuler le plus d'œufs possible pour accélérer le pentagénérateur du cyclope !",

  'Digestomax' : "Pioupiou est prisonnier d'un verger sauvage maudit. Pour se sauver il doit regrouper les fruits qui l'entourent grâce à sa prodigieuse capacité d'absorption.<br> Attention cependant, un fruit de trop et c'est l'indigestion !",

  'Drakhan': "Faites tourner la roue du Drakhan pour éliminer les grappes de perles précieuses. Attention, le dragon ne tolère aucune perle en dehors du plateau !",

  'El Tortuga Nemesis' : "El Tortuga, général tortue vétéran, a inventé un procédé de tonte révolutionnaire pour sauver ses congénères : la tonte laser par quadrillage de zone.<br> Malheureusement ses premiers essais sont mis à rude épreuve par ses ennemis jurés les écureuils fous au dents d'aciers (qui prennent les tortues pour des noix géantes) et par Space Epice, le chien extraterrestre belliqueux !",

  'Electrolink' : "Découvrez les joies des expériences scientifiques avec la machine à réactions électrolytiques !<br>Cliquez sur les dalles du plateau central pour relier les diodes de gauche à celles de droite et provoquer des réactions en chaîne.",

  'Ellon In The Dark' : "Aidez Ellon, l'apprenti sorcier, à lutter contre les forces du Noir Absolu en balayant les hordes de monstres qui déferlent sur Londres.<br>A moins que tout ceci ne soit le fruit de son imagination...",

  'F1 Champion' : "Envie de folles vitesses ? Prenez le volant d'une Formule 1 et négociez les chicanes en ramassant les bonus. Attention au virage !",

  'FAFI 360' : "Aidez vos joueurs quelque peu distraits à viser correctement afin de marquer un maximum de buts sans vous faire prendre la balle par l’équipe adverse !<br><br>Attention, plus votre équipe réussit de passes plus celle-ci sera... distraite :)<br><br>En mode League, enchaîner des passes sans vous faire prendre la balle vous amènera la gloire !",

  'Flushee' : "Ces petits animaux sont très fragiles : à peine on les effleure et ils explosent en grappes ! Gagnez le maximum de points en un nombre limité de coups.",

  'Green Witch' : "Des donjons, des Princes capturés, aidez ces pauvres bougres à s'enfuir avant qu'ils ne meurent de faim !",

  'Happy Pti Tank' : "La brigade heureuse 69 de l'Happy pti tank a encore usé de substances illicites lors de sa dernière 'pride et elle se retrouve non sans surprise au milieu d'un essai d'arme col-horrifique.<br>Aidez le Happy pti tank à s'enfuir au plus vite de ce mauvais trip en éliminant un maximum de vieux machins bizarres. Flower pow@",

  'Hexile' : "C'est la guerre dans L'archipel de Sokopoulos<br>Anéantissez l'armée bleue et prenez le contrôle de leurs troupes pour remporter la victoire !",

  'Hypercube' : "Ramassez les pièces qui défilent et rassemblez-les pour former des carrés et ainsi gagner un max de points ! Ce jeu va vous rendre carrément fou !",

  'IceWay' : "Vous êtes Emma et le froid a envahi les lieux, rendant le sol très glissant ! Vous devez aider votre frère à rentrer à la maison.<br><br>Mais attention vous ne pouvez pas vous arrêter où vous voulez, rendant les trajets difficiles à planifier.<br><br>Il ne suffit donc pas de passer à un endroit, il faut pouvoir vous y arrêter !<br><br>Sortirez-vous à temps de ce labyrinthe, sauverez-vous votre petit frère de la tempête de neige qui s'annonce?",

  'Interwheel' : "Au secours c'est l'inondation, aidez Krakra la petite tache de crasse à s'évader de la salle de bain du temple aztèque Tenochtitlan. Attention aux mines ancestrales du grand Quetzal !",

  'Invasion' : "Les hordes de blob-fantômes urticants déferlent dans les plaines du Shang-Xi. Aidez les guerriers Bukoïen à se déployer pour stopper l'invasion, puis... contre-attaquez !!",

  'Iron Chouquette' : "La patrouille des lapins-robots a kidnappé chouquette ! La disparition de la mascotte de l'équipe d'iron-crocket, pourrait entrainer sa disqualification du tournoi interstellaire d'Andromède. Retrouvez-la avant qu'il ne soit trop tard !!",

  'Judo Commando' : "L'armée du général Setcher a mis au point un procédé dangereux permettant de transformer de stupides chimpanzés en véritables machines de guerre. Vous êtes Jack Klimax judoka 5ème dan spécialiste de l'infiltration. Pénétrez discrètement dans les locaux du général et ramenez des preuves de ses vils agissements.",

  'Julianus' : "Aidez Julianus le nuage à pousser sa petite bulle en évitant les méchants pics et les vilains ventilateurs, tout en ramassant les gentils bonus.",

  'K-Slash !' : "Kanji est en infiltration au pays des bambous ! Débarrassez-vous de ces encombrantes tortues belliqueuses grâce à vos shurikens et votre sabre, ou contentez-vous de les aplatir comme des crêpes en leur sautant dessus !",

  'K-Train' : "Foncez, ramassez et ne vous retournez jamais :)",

  'Kanji' : "Kanji le ninja saura-t-il se défaire des pièges que renferme ce Zoo infernal ? Dans l'antique Japon, les animaux n'étaient pas toujours l'ami de l'homme : évitez-les en sautant et ramassez les symboles Zen.",

  'Kanji Gaiden' : "La forêt sacrée des banouniers géants est envahie par un groupe de Wizz-titis !<br> Chasse vite ces pillards avant qu'il n'y ait plus assez de banounes pour le fameux cake Choco-banoune de Lili. <br> Attention les Wizz-titis sont rapides, rusés et particulièrement énervants ! C'est le moment idéal pour mettre à profit ton entraînement au shuriken.",

  "Kanji's Adventure" : "Comme tous les ans Kanji doit nettoyer sa cave. Malheureusement cette année les monstres ont l'air particulièrement agressifs...<br> Essayez d'en neutraliser un maximum avant de manquer de nourriture !",

  "Kanji's Nightmare" : "Aidez Kanji à sortir de cet affreux cauchemar ! Pourfendez vos ennemis, récupérez un maximum de bols de ramen et ne traînez pas en chemin... La méduse rôde !",

  'Kaskade 2' : "Kaskade est de retour ! Détruisez les groupes de même couleur les plus grands possibles pour marquer un max de points. Mais attention, vous avez seulement 20 coups !",

  'Kavern' : "Aidez PiouPiou à explorer de sombres cavernes ! Ramassez des légumes pour gagner des points et des capsules d'énergie pour survivre plus longtemps.",

  'Kill Bulle' : "Retrouvez Kanji le Ninja dans une nouvelle aventure ! Utilisez le grappin de façon à détruire les bulles bondissantes et gagnez ainsi un max de points.",

  'Klinker Surprise' : "La plateforme orbitale Klinker a subi une importante panne d'énergie. Reliez un à un les capteurs chromatiques pour relancer les moteurs et avoir une surprise !",

  'Linea' : "Premier jeu éducatif sur KadoKado, Linea vous permettra de tester votre motricité au travers d'une petite course d'obstacles qui vous fera hurler de ...",

  "Logic'0" : "Empêchez le cercle de s'élargir en regroupant les billes de couleur par groupes de quatre. Si les billes quittent le cercle, c'est perdu !",

  'Magmax' : "De nombreux ennemis vous assaillent de tous les cotés, survivez le maximum de temps en enfer en les refroidissant !",

  'Manda' : "Tortillez-vous pour ramasser les fruits et les bonus, tentez d'obtenir le Jackpot et surtout évitez les murs ! Un grand classique.",

  'Mel' : "DesCity, le 27 Février 4242<br/><br/>##Communication cryptée interceptée par Black$ter Surveillance##<br/><br/>Le groupe de dissidents Pon3d doit être mâté. Ils nous ont infiltrés, étudiés, ils connaissent notre faiblesse. Ils ont volé les plans de l'usine de sKrill. Il faut à tout prix les arrêter. DesCity survivra à l'insurrection !<br/><br/>Les seize veillent sur l'Ordre.<br/><br/>##FIN DE TRANSMISSION##<br/><br/>Vous incarnez Mel, dernière recrue de la résistance. Vos aptitudes acrobatiques sont incroyables. La guerre numérique est impossible contre Black$ter, restent ces bons vieux messages de papier.<br/>En tant que courrier de la résistance, vous portez la pire arme contre le régime.<br/> L'information libre !",

  'Mini-Race' : "A plus de 200cm/heure, menez votre bolide vers la victoire en prenant garde de ne pas mouiller vos pneus !",

  'Nosushi' : "Réunissez les aliments en cliquant avec votre souris sur des ensembles composés d'au moins deux aliments d'un même type.<br/>Vous avez vingt coups (compteur d'assiettes en bas à droite) avant la fin de la partie. Vous pourrez gagner des coups supplémentaires au cours du jeu.<br/><br/>Créez le plus gros ensemble d'aliments pour multiplier le nombre de points gagnés !",

  'Opalus': "Trouvez votre chemin dans le labyrinthe d'Opalus en détruisant les pierres précieuses d'une même couleur. Plus vous agrandissez votre espace de jeu, plus vous marquez des points !",

  'Opalus Factory': "L'Opalus Factory, la célèbre entreprise de jetons colorés, cherchait quelqu'un pour s'occuper du tri sur sa nouvelle chaîne de fabrication. A la surprise générale, Pioupiou a eu le poste ! <br/>Aide-le à fournir des jetons pour les milliards de joueurs d'Opalus qui piaffent d'impatience !",

  'Oursouinvader' : "Un gentil petit Oursouin s'est installé dans le fond d'un évier riche en substances nutritives. <br> Détruisez les méchants agents blanchissants venus assainir votre microcosme.",

  'Overdrive' : "John Struck a trahi l'Organisation, il ne lui reste qu'une chose possible...<br/>Fuir le plus loin possible !<br/>Mais tous les hommes de main du Boss sont mobilisés pour l'en empêcher. Heureusement, sa fidèle Camaro est plus rapide que n'importe quel autre véhicule.<br/><br/>Pilotez le bolide de John Struck et frayez-vous un chemin pour espérer survivre à la route qui vous mènera vers la rédemption.",

  'Pacifik' : "Objectifs : <br> Protéger chaque armée l'une contre l'autre ! <br> Protéger les convois de secours <br> Conditions de perte : <br> Une des armée dévastée <br> Un convoi de secours touché 2x",

  'Paradice' : "Les vents du nord ont gelé la plage d'Hokitika !! Heureusement la division Chasse-neige de l'armée Pingouin va y mettre de l'ordre. <br> Aidez-les à dégeler les cocotiers en éliminant tous les blocs de glace !",

  'Phagocytoz' : "Une invasion de scolomorphes roses met en danger les ganglions tropicaux de notre secteur. Vous devez phagocyter l'ensemble des scolomorphes ainsi que les cellules présentes pour éviter tout risque d'infection !",

  'Piou-Piou' : "Aidez le pauvre Piou-Piou tombé dans cet affreux piège digne des plus machiavéliques écraseurs de poussins.",

  'Pioutch' : "Faites rebondir les Pious jusqu'à la droite de l'écran ! A peine sauvés des niveaux psychopathes de PiouPiouz.com, Kanji est chargé de mettre les Pious en quarantaine pour les protéger de la grippe aviaire.",

  'Popcorn' : "L'ignoble Pokepi a fait tomber PiouPiou dans une crevasse profonde et il essaie de l'étouffer sous une pluie de popcorns !! Aidez PiouPiou à vaincre Pokepi avant que le popcorn n'atteigne la limite !",

  'Pulsar' : "'Avé Pulsar ! Ceux qui vont mourir te saluent !'<br>Vous voilà dans l'arène du grand tournoi intergalactique Pulsar ! Le plus important n'est pas de savoir si vous allez survivre, mais combien de points vous allez gagner !<br><br>Mode Level-Up<br><br>Tentez de survivre jusqu’au prochain niveau en obtenant le plus de points possible.<br>Entre chaque niveau, choisissez les améliorations de... vos ennemis !<br><br>Mode League<br><br>Survivez le plus longtemps possible et faites un meilleur score que vos amis !<br>Ramassez des options pour gagner en puissance",

  'Punch-In !' : 'Affrontez le célèbre champion "Gold-o-Lingot" dans un match de boxe historique ! Attention à ne pas perdre votre souffle !',

  'Puzzle-Manda' : "Après une sévère indigestion, manda est finalement de retour ! Il devra désormais suivre les conseils avisés d'un nutritionniste afin de conserver ses courbes soOo snaxy !",

  'Quadrikolor' : "Il faudra faire preuve d'habileté dans ce jeu de billard cosmique. Utilisez votre vaisseau spatial pour envoyer les aliens colorés dans les quatre coins, mais attention aux trous noirs !",

  'Razor' : "Composez, grâce à votre scie circulaire, le meilleur cocktail de monstres possible et tentez de comprendre par vous-même les obscures conditions de fin de partie.",

  'Red-Raid' : "Commandez les marines surentraînés de l'escouade Red et exterminez tous les insectes qui oseront pénétrer dans le secteur 03. Repoussez-les le plus longtemps possible en attendant les renforts.",

  'Rock Faller' : "Assemblez vos gemmes par 4 pour marquer des points et créer des réactions en chaîne.<br>Des petits malins ont glissé des cailloux sans valeur dans votre collection... Faites-les disparaître par les tuyaux pour obtenir des bonus.",

  'Safari' : "Les vaisseaux aliens pullulent dans la jungle. Ajustez votre mitrailleuse, dégommez les tous et ramenez des trophées pour votre cheminée.",

  'Shizo Fuzz' : "Fuzz le renard schizophrène envie les écureuils volants. Ne cherchez pas trop à comprendre... mais vous pouvez toujours l'aider à réaliser son rêve.",

  'Spiroule' : "Suite aux augmentations de cadence infernale imposées par une société consumériste et décadente, le générateur de fantasiotes s'est emballé et nul ne semble pouvoir l'arrêter ! Détruisez un maximum de fantasiotes en les regroupant par lignes de trois de la même couleur.",

  'Starfang' : "A bord de votre Starfang DCX-40, Croquez les météores de la galaxie et échappez à la police intersidérale de protection des météores à grands coups d'hyperthruster !",

  'Synapses' : "Après un réveil difficile causé par une soirée trop arrosée, Rémi le punk doit tenter de remettre son cerveau en place pour partir au travail. <br> Aidez-le en reconnectant toutes les synapses décollées pendant la nuit.",

  'Tianan Man' : "Un méchant général vient de faire un coup d'état ! Heureusement, Tianan Man le super manifestant et son superpouvoir d'arrêter le temps sont là pour montrer que la populace a son super mot à dire !",

  'Tout-Caen' : "Papoyo le toucan vagabond s'est perdu lors d'une migration improvisée vers la Suède. <br> Il est maintenant perdu au-dessus du port de Caen et il a très faim !<br> Aidez-le à plonger pour trouver les bancs de délicieux poissons.",

  'Toy Maniak' : "Vous avez déjà rêvé de diriger une usine à jouets ? Dans Toy Maniak, vous êtes aux commandes ! Rassemblez les jouets identiques sur la même ligne.",

  'Travoltax' : "Entrez dans la révolution psychéludique de Travoltax ! <br> Organisez les blocs qui tombent du ciel pour faire de jolies lignes bien droites, sans aucun trou.",

  'Trigo' : "Formez un premier triangle en cliquant sur trois cases de la même couleur. Puis formez un triangle plus grand autour du premier.<br><br>A chaque triangle formé, vos points seront démultipliés !<br><br>Lorsque vous ne pouvez plus agrandir, cliquez sur le triangle pour obtenir les points et récupérer un peu de temps pour les triangles suivants.",

  "Twin Spirit" : "Vous dirigez l'escadron de choc TwinSpirit composé des pilotes Wallis et Futuna. <br> Lors d'une mission de routine, l'appareil de Wallis est abattu par Robert. <br> Grâce à son moteur temporel, Futuna parvient à remonter le temps pour prêter main forte à son coéquipier. <br> Parviendrez-vous à intercepter et détruire Robert à temps ?",

  'Xian-Xiang' : "Découvrez les mystères de ce jeu d'origine chinoise : vous devez faire correspondre symboles, formes et couleurs pour ramasser le plus de points.",

  'ZipZap' : "L'abeille tueuse de Kanji est de retour ! <br> Cette fois vous allez devoir l'aider : éclatez les 100 ballons colorés avec son dard. <br> Alignez plusieurs ballons de la même couleur pour gagner plus de points."

};
