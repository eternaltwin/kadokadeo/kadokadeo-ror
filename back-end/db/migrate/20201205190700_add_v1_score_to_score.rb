class AddV1ScoreToScore < ActiveRecord::Migration[6.0]
  def change
    add_column :scores, :v1_score, :integer
  end
end
