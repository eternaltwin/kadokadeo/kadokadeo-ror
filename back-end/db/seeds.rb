# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).

games = [
  {
    name: 'ABC Revolution',
    mode: :action,
  },
  {
    name: 'Opalus Factory',
    mode: :puzzle,
  },
  {
    name: 'Alphabounce',
    mode: :action,
  },
  {
    name: 'Iron Chouquette',
    mode: :action,
  },
  {
    name: 'K-Slash !',
    mode: :action,
  },
  {
    name: 'Kanji',
    mode: :action,
  },
  {
    name: 'Ellon In The Dark',
    mode: :action,
  },
  {
    name: 'Interwheel',
    mode: :action,
  },
  {
    name: 'Piou-Piou',
    mode: :action,
  },
  {
    name: 'Manda',
    mode: :action,
  },
  {
    name: 'Schizo Fuzz',
    mode: :action,
  },
  {
    name: 'Mini-Race',
    mode: :action,
  },
  {
    name: 'Kavern',
    mode: :action,
  },
  {
    name: 'Twin Spirit',
    mode: :action,
  },
  {
    name: 'Judo Commando',
    mode: :action,
  },
  {
    name: 'Tianan Man',
    mode: :action,
  },
  {
    name: 'ZipZap',
    mode: :action,
  },
  {
    name: 'Kill Bulle',
    mode: :action,
  },
  {
    name: 'Starfang',
    mode: :action,
  },
  {
    name: 'Oursouinvader',
    mode: :action,
  },
  {
    name: 'Cyclopean',
    mode: :action,
  },
  {
    name: 'Punch-In !',
    mode: :action,
  },
  {
    name: 'Crepuscud',
    mode: :action,
  },
  {
    name: "Kanji's Nightmare",
    mode: :action,
  },
  {
    name: 'Chakré Bouddha',
    mode: :action,
  },
  {
    name: 'F1 Champion',
    mode: :action,
  },
  {
    name: 'Tout-Caen',
    mode: :action,
  },
  {
    name: 'El Tortuga nemesis',
    mode: :action,
  },
  {
    name: 'K-Train',
    mode: :action,
  },
  {
    name: 'Cosmo Crash',
    mode: :action,
  },
  {
    name: 'Magmax',
    mode: :action,
  },
  {
    name: 'Red Raid',
    mode: :action,
  },
  {
    name: 'Pioutch',
    mode: :action,
  },
  {
    name: 'Safari',
    mode: :action,
  },
  {
    name: 'Pacifik',
    mode: :action,
  },
  {
    name: 'Phagocytoz',
    mode: :action,
  },
  {
    name: 'Linea',
    mode: :action,
  },
  {
    name: 'Kanji Gaiden',
    mode: :action,
  },
  {
    name: 'Julianus',
    mode: :action,
  },
  {
    name: 'Popcorn',
    mode: :action,
  },
  {
    name: 'Happy Pti Tank',
    mode: :action,
  },
  {
    name: 'Flushee',
    mode: :puzzle,
  },
  {
    name: 'Opalus',
    mode: :puzzle,
  },
  {
    name: 'Kaskade 2',
    mode: :puzzle,
  },
  {
    name: 'Cooking Lili',
    mode: :puzzle,
  },
  {
    name: 'Xian-Xiang',
    mode: :puzzle,
  },
  {
    name: 'Aqua Splash',
    mode: :puzzle,
  },
  {
    name: 'Atlanteine',
    mode: :puzzle,
  },
  {
    name: "Logic'O",
    mode: :puzzle,
  },
  {
    name: 'Choco-Mouche',
    mode: :puzzle,
  },
  {
    name: 'Alchimie',
    mode: :puzzle,
  },
  {
    name: 'Bactery',
    mode: :puzzle,
  },
  {
    name: 'Binary',
    mode: :puzzle,
  },
  {
    name: 'QuadriKolor',
    mode: :puzzle,
  },
  {
    name: 'Hexile',
    mode: :puzzle,
  },
  {
    name: "Kanji's Adventure",
    mode: :puzzle,
  },
  {
    name: 'Razor',
    mode: :puzzle,
  },
  {
    name: 'Spiroule',
    mode: :puzzle,
  },
  {
    name: 'Cereal Punk',
    mode: :puzzle,
  },
  {
    name: 'Travoltax',
    mode: :puzzle,
  },
  {
    name: 'Synapses',
    mode: :puzzle,
  },
  {
    name: 'Paradice',
    mode: :puzzle,
  },
  {
    name: 'Invasion',
    mode: :puzzle,
  },
  {
    name: 'Electrolink',
    mode: :puzzle,
  },
  {
    name: 'Toy Maniak',
    mode: :puzzle,
  },
  {
    name: 'Drakhan',
    mode: :puzzle,
  },
  {
    name: 'Digestomax',
    mode: :puzzle,
  },
  {
    name: 'Puzzle-Manda',
    mode: :puzzle,
  },
  {
    name: 'Autrement',
    mode: :puzzle,
  },
  {
    name: 'Hypercube',
    mode: :puzzle,
  },
  {
    name: 'Klinker Surprise',
    mode: :puzzle,
  },
  {
    name: 'Rock Faller',
    mode: :puzzle,
  },
  {
    name: 'Animoz',
    mode: :puzzle,
  },
  {
    name: 'Nosushi',
    mode: :puzzle,
  },
  {
    name: 'BattleSheep',
    mode: :puzzle,
  },
  {
    name: 'Trigo',
    mode: :puzzle,
  },
  {
    name: 'IceWay',
    mode: :puzzle,
  },
  {
    name: 'BadaBloom',
    mode: :puzzle,
  },
  {
    name: 'Cataclismo',
    mode: :puzzle,
  },
  {
    name: 'Brutal Teenage Crisis',
    mode: :action,
  },
  {
    name: 'Fafi 360',
    mode: :action,
  },
  {
    name: "Charlotte's Quest",
    mode: :action,
  },
  {
    name: 'Capman',
    mode: :action,
  },
  {
    name: 'Pulsar',
    mode: :action,
  },
  {
    name: 'Mel',
    mode: :action,
  },
  {
    name: 'Overdrive',
    mode: :action,
  },
  {
    name: 'Green Witch',
    mode: :action,
  },
]

games.each do |game|
  Game.find_or_create_by(**game)
end
