export default {
  fr: {
    menu: {
      account: 'mon Compte',
      games: 'jeux',
      site: 'site',
      clan: 'clan',
      forum: 'forum',
    },
    games: {
      favorites: 'favoris',
      action: 'action',
      puzzle: 'reflexion',
      search: 'Rechercher...',
      show: {
        infos: 'Infos',
        rules: 'Règles',
        score: 'Score',
        scoreError: 'Une erreur est survenue :(',
        ranking: 'Classement',
        scores: {
          highest: 'Record : ',
          personal: 'Record Personnel : ',
          period: 'Période : ',
        },
      },
    },
    levels: {
      trophies: 'Trophées',
      name: 'niveau',
      paradise: 'paradis',
      gold: 'or',
      silver: 'argent',
      bronze: 'bronze',
      red: 'rouge',
      orange: 'orange',
      green: 'vert',
      confirmed: 'confirmé',
      newbie: 'débutant',
    },
    sidebar: {
      kalendar: {
        title: 'kalendrier',
        period: 'période',
        day: 'jour',
      },
      stars: {
        title: 'étoiles',
      },
      clansRank: {
        title: 'top clans',
        attackRadioLabel: 'A/D',
        missionRadioLabel: 'mission',
        fullRankings: 'classements complets',
      },
      rankings: {
        title: 'Classements'
      },
    },
    auth: {
      signIn: 'se connecter',
      signUp: "s'inscrire",
      password: 'Mot de passe',
      submit: 'envoyer',
      signUpSuccessful: 'inscription réussie !',
      signUpError: 'une erreur est survenue',
      signInError: 'identifiants incorrectes',
    },
    ranking: {
      player: 'Joueur',
      score: 'Score',
      date: 'Date',
      game: 'Jeu',
      v1: 'V1',
      v1_total: 'V1 total',
      feathers: 'Plumes',
    },
    user: {
      didNotPlay: "Ce joueur n'a pas encore joué cette période.",
      completion: 'Récoltez toutes les étoiles rouges pour atteindre la valeur maximale de la barre !',
      search: 'Rechercher un joueur',
      tab: {
        scores: 'Scores',
        stats: 'Stats',
        statsTable: {
          best: 'Record',
        },
      },
      v1RankTooltip: 'Rang au classement V1',
    },
    steps: {
      title: 'Paliers des Jeux',
      game: 'Jeu',
      green: 'Étoile Verte',
      orange: 'Étoile Orange',
      red: 'Étoile Rouge',
      max: '1500',
    },
    feathersRanking: {
      label: 'Poids Plumes',
    },
  },
  en: {
    menu: {
      account: 'my Account',
      games: 'games',
      site: 'site',
      clan: 'clan',
      forum: 'forum',
    },
    games: {
      favorites: 'favorites',
      action: 'action games',
      puzzle: 'puzzle games',
      search: 'Search...',
      show: {
        infos: 'Info',
        rules: 'Rules',
        score: 'Score',
        scoreError: 'Something wrong happened :(',
        ranking: 'Ranking',
        scores: {
          highest: 'Highest Score:',
          personal: 'Personnel Best:',
          period: 'Period:',
        },
      },
    },
    levels: {
      trophies: 'Trophies',
      name: 'level',
      paradise: 'paradise',
      gold: 'gold',
      silver: 'silver',
      bronze: 'bronze',
      red: 'red',
      orange: 'orange',
      green: 'green',
      confirmed: 'confirmed',
      newbie: 'newbie',
    },
    sidebar: {
      kalendar: {
        title: 'kalendar',
        period: 'period',
        day: 'day',
      },
      stars: {
        title: 'stars',
      },
      clansRank: {
        title: 'top clans',
        attackRadioLabel: 'A/D',
        missionRadioLabel: 'mission',
        fullRankings: 'full rankings',
      },
      rankings: {
        title: 'Rankings'
      },
    },
    auth: {
      signIn: 'sign in',
      signUp: 'sign up',
      password: 'Password',
      submit: 'submit',
      signUpSuccessful: 'sign up successful!',
      signUpError: 'an error has occured',
      signInError: 'invalid credentials',
    },
    ranking: {
      player: 'Player',
      score: 'Score',
      date: 'Date',
      game: 'Game',
      v1: 'V1',
      v1_total: 'Total V1',
      feathers: 'Feathers',
    },
    user: {
      didNotPlay: 'This player did not play yet this period.',
      completion: 'Gather every red stars to fill this bar!',
      search: 'Search a player',
      tab: {
        scores: 'Scores',
        stats: 'Stats',
        statsTable: {
          best: 'Best Score',
        },
      },
      v1RankTooltip: 'V1 ranking position',
    },
    steps: {
      title: 'Game Steps',
      game: 'Game',
      green: 'Green Star',
      orange: 'Orange Star',
      red: 'Red Star',
      max: '1500',
    },
    feathersRanking: {
      label: 'Feathers Ranking',
    },
  },
};
