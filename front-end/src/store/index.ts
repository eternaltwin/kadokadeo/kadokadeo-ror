import Vue from 'vue'
import Vuex from 'vuex'

import { get } from 'lodash';

Vue.use(Vuex)

export const USER = 'user';
export const AUTH = 'auth';
export const SET_LOADING = 'setLoading';
export const SET_GAMES = 'setGames';
export const SET_STARS = 'setStars';

export default new Vuex.Store({
  state: {
    user: null,
    auth: {},
    loading: false,
    games: [],
    userStars: null,
  },
  getters: {
    user: state => state.user,
    auth: state => state.auth,
    isLogged: state => !!state.user,
    userEmail: state => get(state, ['user', 'email']),
    userStars: state => state.userStars,
  },
  mutations: {
    [USER](state, value) {
      state.user = value;
    },
    [AUTH](state, value) {
      state.auth = value;
    },
    [SET_LOADING](state, value) {
      state.loading = value;
    },
    [SET_GAMES](state, value) {
      state.games = value;
    },
    [SET_STARS](state, value) {
      state.userStars = value;
    },
  },
  actions: {
  },
  modules: {
  }
})
