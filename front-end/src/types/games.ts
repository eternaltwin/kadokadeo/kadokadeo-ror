import StepNames from '@/components/pages/Steps.vue';

type GameMode = 'action' | 'puzzle';

type GameName = "ABC Revolution" | "Alchimie" | "Alphabounce" | "Animoz" | "Aqua Splash" | "Atlanteine" | "Autrement" | "Bactery" | "BadaBloom" | "BattleSheep" | "Binary" | "Brutal Teenage Crisis" | "Capman" | "Cataclismo" | "Cereal Punk" | "Chakré Boudha" | "Charlotte's Quest" | "Choco-Mouche" | "Cooking Lili" | "Cosmo Crash" | "Crepuscud" | "Cyclopean" | "Digestomax" | "Drakhan" | "El Tortuga Nemesis" | "Electrolink" | "Ellon In The Dark" | "F1 Champion" | "FAFI 360" | "Flushee" | "Green Witch" | "Happy Pti Tank" | "Hexile" | "Hypercube" | "IceWay" | "Interwheel" | "Invasion" | "Iron Chouquette" | "Judo Commando" | "Julianus" | "K-Slash !" | "K-Train" | "Kanji" | "Kanji Gaiden" | "Kanji's Adventure" | "Kanji's Nightmare" | "Kaskade 2" | "Kavern" | "Kill Bulle" | "Klinker Surprise" | "Linea" | "Logic'0" | "Magmax" | "Manda" | "Mel" | "Mini-Race" | "Nosushi" | "Opalus" | "Opalus Factory" | "Oursouinvader" | "Overdrive" | "Pacifik" | "Paradice" | "Phagocytoz" | "Piou-Piou" | "Pioutch" | "Popcorn" | "Pulsar" | "Punch-In !" | "Puzzle-Manda" | "Quadrikolor" | "Razor" | "Red-Raid" | "Rock Faller" | "Safari" | "Shizo Fuzz" | "Spiroule" | "Starfang" | "Synapses" | "Tianan Man" | "Tout-Caen" | "Toy Maniak" | "Travoltax" | "Trigo" | "Twin Spirit" | "Xian-Xiang" | "ZipZap";

interface Game {
  id: number;
  name: GameName;
  mode: GameMode;
  pinned: boolean;
  steps?: { name: StepNames; points: number;
            value: number }[];
}

export {
  GameMode,
  GameName,
  Game,
}
