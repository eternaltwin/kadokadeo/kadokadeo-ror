class Score < ApplicationRecord
  include StepsConcern
  include LevelsConcern
  enum star: [:grey, :green, :orange, :red]
  belongs_to :user
  belongs_to :game
  belongs_to :period

  attribute :period_id, :integer, default: -> { Period.last.id }

  scope :period, -> { where(period: Period.last) }

  before_create do
    game_steps = self.game.steps
    self.v1_score = compute_v1_score(game_steps: game_steps)
    self.star = compute_star(game_steps: game_steps)
  end

  after_create do
    user_v1_total = V1Total.find_by(user: self.user, period: Period.last)
    if !user_v1_total
      V1Total.create(user_id: self.user_id, period_id: Period.last.id,
                     value: user.compute_v1_total)
    else
      user_v1_total.update(value: user.compute_v1_total)
    end
  end

  def self.best_by_user(game:, period: false)
    select('row_number() OVER (ORDER BY value DESC) as rank',
           'users.id AS "user_id"', :email,
           :value, 'scores.created_at')
      .joins(user_join_highscore_query(game: game, period: period))
  end

  def self.game_ranking_by_level(game:, level_int:)
    Score.period
         .select('row_number() OVER (ORDER BY MAX(value) DESC) as rank',
                 'MAX(value) as value',
                 'users.id AS "user_id"',
                 :email)
         .left_joins(:user)
         .joins("LEFT JOIN levels
                 ON levels.user_id = users.id
                 AND levels.game_id = scores.game_id")
         .where(game_id: game.id)
         .where('levels.name = ?',
                 level_int)
         .group(:game_id, 'users.id', :email)
  end

  def period_rank
    # rank for user highscore for a game
    # /!\ not necessarily for current score instance
    level_int = self.user.level_for_game(self.game)
                         .read_attribute_before_type_cast(:name)
    self.class.unscoped
              .select(:rank)
              .from(self.class.game_ranking_by_level(game: self.game,
                                                     level_int: level_int))
              .where('"user_id" = ?', self.user_id)
              .limit(1)[0].rank
  end

  private

  def compute_star(game_steps: nil)
    game_steps ||= self.game.steps
    game_steps.pop
    next_step_index = game_steps.index{ |step| step['value'] > self.value }

    stars = self.class.stars.keys # this includes the 'grey star'
    return stars.last unless next_step_index
    stars[next_step_index]
  end

  def compute_v1_score(game_steps: nil)
    game_steps ||= self.game.steps
    next_step_index = game_steps.index{ |step| step['value'] > self.value }
    return 1500 unless next_step_index

    current_step = game_steps[next_step_index - 1] || { value: 0, points: 0 }
    score = current_step['points']
    factor = (self.value - current_step['value']) / (game_steps[next_step_index]['value'] - current_step['value']).to_f
    score += factor * (game_steps[next_step_index]['points'] - current_step['points'])
    score.floor
  end

  def self.user_join_highscore_query(game:, period: false)
    user_highscore_query = "INNER JOIN users
                            ON scores.user_id = users.id
                            AND scores.value =
                            (SELECT MAX(value)
                            FROM scores
                            WHERE game_id = #{game.id}
                            AND scores.user_id = users.id"
    if period
      user_highscore_query += " AND scores.period_id = #{Period.last.id}"
    end
    user_highscore_query + ')'
  end
end
