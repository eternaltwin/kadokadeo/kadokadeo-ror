class FavouritesController < ApplicationController
  before_action :authenticate_user!

  def toggle
    game = Game.find(params[:id])
    favourite = Favourite.find_by(game: game, user: current_user)

    if favourite
      favourite.destroy
      status = :ok
    else
      current_user.favourites.create(game: game)
      status = :created
    end

    render nothing: true, status: status
  end
end
