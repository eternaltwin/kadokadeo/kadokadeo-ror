import { GameName } from '@/types/games';

export enum Star {
  GREY = 'grey',
  GREEN = 'green',
  ORANGE = 'orange',
  RED = 'red',
}

export type StarObject = Omit<Record<Star, number>, Star.GREY>;

export interface Score {
  name: GameName;
  game_id?: number;
  value: number;
  star?: Star;
  period_rank: number;
  v1_score: number;
}

export enum Trophy {
  PARADISE = 'paradise',
  GOLD = 'gold',
  SILVER = 'silver',
  BRONZE = 'bronze',
  RED = 'red',
  ORANGE = 'orange',
  GREEN = 'green',
  CONFIRMED = 'confirmed',
  NEWBIE = 'newbie',
}

export type Trophies = Partial<Record<Trophy, number>>;
